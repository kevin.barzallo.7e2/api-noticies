import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment.prod';
import { RespuestaNoticias } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getNoticias(){

    let key = environment.apiKey;
    
    return this.http.get<RespuestaNoticias>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=${key}`);
  }

  getNoticiasCategoria(category: any){
    let key = environment.apiKey;
    
    return this.http.get<RespuestaNoticias>(`https://newsapi.org/v2/top-headlines?language=es&category=${category}&apiKey=${key}`);

  }
}
