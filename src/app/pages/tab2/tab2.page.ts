import { Component, OnInit } from '@angular/core';
import { Article } from '../../interfaces/interfaces';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  selectedCategoria:string = "general";
  noticies:Article[] = [];
  categories:string[] = ["general","business","entertainment","health","science","sports","technology"]
  constructor(private data: DataService) {}

  loadData(event){
    this.loadNoticies(event);
  }

  loadNoticies(event?){
    this.data.getNoticiasCategoria(this.selectedCategoria).subscribe(
      resp => {
        if(resp.status === "error"){
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.noticies.push(...resp.articles);
        if(event){
          event.target.complete();
        }
      }
    )
  }

  segmentChanged(ev: any) {
    this.selectedCategoria = ev.detail.value;
    this.noticies = [];
    this.loadNoticies()
    
  }

  

  ngOnInit(): void {
    this.loadNoticies();
  }
}

