import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  noticies:Article[] = [];
  constructor(private data: DataService) {}

  loadData(event){
    this.loadNoticies(event);
  }

  loadNoticies(event?){
    this.data.getNoticias().subscribe(
      resp => {
        if(resp.status === "error"){
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.noticies.push(...resp.articles);
        if(event){
          event.target.complete();
        }
      }
    )
  }

  ngOnInit(): void {
    this.loadNoticies();
  }

}
