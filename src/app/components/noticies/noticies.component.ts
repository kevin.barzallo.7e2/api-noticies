import { Component, Input, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { DataLocalService } from 'src/app/services/data-local.service';

const { Browser } = Plugins;
const { Share } = Plugins;

@Component({
  selector: 'app-noticies',
  templateUrl: './noticies.component.html',
  styleUrls: ['./noticies.component.scss'],
})
export class NoticiesComponent implements OnInit {

  @Input() noticies: Article[] = [];

  constructor(public actionSheetController: ActionSheetController, public dataStorage: DataLocalService) { 
    Browser.addListener('browserPageLoaded', () => {
      console.log('browserPageLoaded event called');
    });
    Browser.addListener('browserFinished', () =>{
      console.log('browserFinished event called');

    });

    Browser.prefetch({
      urls:['https://www.youtube.com/']
    });


  }

  ngOnInit() {
    
  }

  async openNews(url:string){
    await Browser.open({toolbarColor:"#fcba03", url: url});
  }

  async shareNew(noticia: Article){
    await Share.share({
      title: 'Oye mira esto!',
      text: noticia.title,
      url: noticia.url,
      dialogTitle: 'Share with buddies'
    });
  }

  async lanzarMenu(noticia?:Article){
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: 
      [
        {
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
          this.shareNew(noticia);
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          this.dataStorage.guardarNoticia(noticia);
          console.log("do it");
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
