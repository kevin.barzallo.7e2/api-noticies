import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NoticiesComponent } from './noticies/noticies.component';
import { NoticiaComponent } from './noticia/noticia.component';



@NgModule({
  declarations: [
    NoticiesComponent,
    NoticiaComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    NoticiesComponent,
    NoticiaComponent
  ]
})
export class ComponentsModule { }
